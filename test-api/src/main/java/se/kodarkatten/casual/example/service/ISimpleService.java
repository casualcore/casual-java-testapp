package se.kodarkatten.casual.example.service;

import se.laz.casual.api.buffer.CasualBuffer;

public interface ISimpleService
{
    public String echo( String message );
    public String lowercase( String message );

    public SimpleObject echo( SimpleObject message );
    public SimpleObject lowercase( SimpleObject message );

    public CasualBuffer echo( CasualBuffer buffer );

    public CasualBuffer casualEcho( CasualBuffer buffer );
}
