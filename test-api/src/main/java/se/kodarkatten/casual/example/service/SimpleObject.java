package se.kodarkatten.casual.example.service;

import java.io.Serializable;
import java.util.Objects;

public class SimpleObject implements Serializable
{
    private static final long serialVersionUID = 1L;
    private final String message;

    public SimpleObject( String message )
    {
        this.message = message;
    }

    public String getMessage()
    {
        return this.message;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        SimpleObject that = (SimpleObject) o;
        return Objects.equals(message, that.message);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(message);
    }

    @Override
    public String toString()
    {
        return "SimpleObject{" +
                "message='" + message + '\'' +
                '}';
    }
}
