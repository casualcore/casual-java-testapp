package se.kodarkatten.casual.example.service.order;

import se.laz.casual.api.buffer.type.fielded.annotation.CasualFieldElement;

import java.io.Serializable;
import java.util.Objects;

public class CasualOrder implements Serializable
{
    private static final long serialVersionUID = 1L;

    @CasualFieldElement(name="FML_LONG1")
    private Integer id;
    @CasualFieldElement(name = "FML_LONG2" )
    private Integer version;
    @CasualFieldElement(name="FML_STRING1")
    private String product;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getVersion()
    {
        return version;
    }

    public void setVersion(Integer version)
    {
        this.version = version;
    }

    public String getProduct()
    {
        return product;
    }

    public void setProduct(String product)
    {
        this.product = product;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        CasualOrder that = (CasualOrder) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(version, that.version) &&
                Objects.equals(product, that.product);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, version, product);
    }

    @Override
    public String toString()
    {
        return "CasualOrder{" +
                "id=" + id +
                ", version=" + version +
                ", product='" + product + '\'' +
                '}';
    }
}
