package se.kodarkatten.casual.example.service.order;

import se.laz.casual.api.buffer.type.fielded.annotation.CasualFieldElement;

import java.io.Serializable;
import java.util.Objects;

public class CasualOrderId implements Serializable
{
    private static final long serialVersionUID = 1L;

    @CasualFieldElement( name="FML_LONG1" )
    private Integer id;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        CasualOrderId that = (CasualOrderId) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id);
    }

    @Override
    public String toString()
    {
        return "CasualOrderId{" +
                "id=" + id +
                '}';
    }
}
