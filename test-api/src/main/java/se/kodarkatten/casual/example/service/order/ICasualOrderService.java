package se.kodarkatten.casual.example.service.order;

import java.util.List;

public interface ICasualOrderService
{
    CasualOrder create( CasualOrder order );
    List<CasualOrder> get();
    CasualOrder get( CasualOrderId id );
    CasualOrder update( CasualOrder order );
    void delete( CasualOrderId id );
}
