package se.kodarkatten.casual.example.service.order;

import java.util.List;

public interface IOrderService
{
    Order create( Order order );
    List<Order> get();
    Order get( Integer id );
    Order update( Order order );
    void delete( Integer id );

}
