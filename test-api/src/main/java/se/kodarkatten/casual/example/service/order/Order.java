package se.kodarkatten.casual.example.service.order;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Order implements Serializable
{
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer version;
    private String product;
    private LocalDateTime tstamp;

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getVersion()
    {
        return version;
    }

    public void setVersion(Integer version)
    {
        this.version = version;
    }

    public String getProduct()
    {
        return product;
    }

    public void setProduct(String product)
    {
        this.product = product;
    }

    public LocalDateTime getTstamp()
    {
        return tstamp;
    }

    public void setTstamp(LocalDateTime tstamp)
    {
        this.tstamp = tstamp;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Order order = (Order) o;
        return Objects.equals(id, order.id) &&
                Objects.equals(version, order.version) &&
                Objects.equals(product, order.product) &&
                Objects.equals(tstamp, order.tstamp);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, version, product, tstamp);
    }

    @Override
    public String toString()
    {
        return "Order{" +
                "id=" + id +
                ", version=" + version +
                ", product='" + product + '\'' +
                ", tstamp=" + tstamp +
                '}';
    }
}
