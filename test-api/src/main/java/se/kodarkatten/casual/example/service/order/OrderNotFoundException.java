package se.kodarkatten.casual.example.service.order;

public class OrderNotFoundException extends OrderServiceException
{
    private static final long serialVersionUID = 1L;

    public OrderNotFoundException()
    {
        super();
    }

    public OrderNotFoundException(String message )
    {
        super( message );
    }

    public OrderNotFoundException(Throwable t )
    {
        super( t );
    }

    public OrderNotFoundException(String message, Throwable t )
    {
        super( message, t );
    }

}
