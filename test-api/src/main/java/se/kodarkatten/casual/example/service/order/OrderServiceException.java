package se.kodarkatten.casual.example.service.order;

import javax.ejb.ApplicationException;

@ApplicationException(rollback = true)
public class OrderServiceException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public OrderServiceException()
    {
        super();
    }

    public OrderServiceException( String message )
    {
        super( message );
    }

    public OrderServiceException( Throwable t )
    {
        super( t );
    }

    public OrderServiceException( String message, Throwable t )
    {
        super( message, t );
    }

}
