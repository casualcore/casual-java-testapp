package se.kodarkatten.casual.example.service.remote;

import se.laz.casual.jca.inbound.handler.InboundRequest;
import se.laz.casual.jca.inbound.handler.InboundResponse;

public interface SomethingService
{
    public InboundResponse casualEcho(InboundRequest buffer);
}
