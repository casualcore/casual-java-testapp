package se.kodarkatten.casual.example.service.order

import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalDateTime

class CasualOrderTest extends Specification
{

    @Shared CasualOrder instance
    @Shared int id1 = 1
    @Shared int version1 = 2
    @Shared String product1 = "product name 1"
    @Shared LocalDateTime tstamp1 = LocalDateTime.of( 2017, 12, 11, 13, 14, 15 )

    @Shared int id2 = 3
    @Shared int version2 = 4
    @Shared String product2 = "product name 2"
    @Shared LocalDateTime tstamp2 = LocalDateTime.of( 2017, 11, 11, 13, 14, 15 )


    def setup()
    {
        instance = new CasualOrder()
        instance.setId( id1 )
        instance.setVersion( version1 )
        instance.setProduct( product1 )
    }

    def cleanup()
    {
        instance = null
    }

    def "Get id"()
    {
        expect:
        instance.getId() == id1
    }

    def "Set id"()
    {
        setup:
        int id = 4

        when:
        instance.setId( id )

        then:
        instance.getId() == id
    }

    def "Get version"()
    {
        expect:
        instance.getVersion() == version1
    }

    def "Set version"()
    {
        setup:
        int version = 4

        when:
        instance.setVersion( version )

        then:
        instance.getVersion() == version
    }

    def "Get product"()
    {
        expect:
        instance.getProduct() == product1
    }

    def "Set product"()
    {
        setup:
        String product = 4

        when:
        instance.setProduct( product )

        then:
        instance.getProduct() == product
    }

    def "equals and hashcode"()
    {
        setup:
        CasualOrder instance2 = new CasualOrder()
        instance2.setId( id )
        instance2.setVersion( version )
        instance2.setProduct( product )

        expect:
        instance.equals( instance2 ) == result
        (instance.hashCode() == instance2.hashCode() ) == result

        where:
        id  | version  | product  || result
        id1 | version1 | product1 || true
        id2 | version1 | product1 || false
        id1 | version2 | product1 || false
        id1 | version1 | product2 || false
    }

    def "equals null false"()
    {
        expect:
        ! instance.equals( null )
    }

    def "equals string false"()
    {
        expect:
        ! instance.equals( "" )
    }

    def "equals self true"()
    {
        expect:
        instance.equals( instance )
    }

    def "toString returns value"()
    {
        when:
        String actual = instance.toString()

        then:
        actual.contains( ""+id1 )
        actual.contains( ""+version1 )
        actual.contains( product1 )
    }

}
