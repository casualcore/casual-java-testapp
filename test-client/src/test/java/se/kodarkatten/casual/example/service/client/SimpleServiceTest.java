package se.kodarkatten.casual.example.service.client;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.wildfly.naming.client.WildFlyInitialContextFactory;
import se.kodarkatten.casual.example.service.ISimpleService;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Hashtable;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@Ignore
public class SimpleServiceTest
{
    private static Hashtable<Object,Object> env;
    private static InitialContext context;

    private static String host = "192.168.99.100";
    private static String port  = "8080";

    private static String JNDI_NAME = "ejb:/casual-java-testapp/SimpleService!se.kodarkatten.casual.example.service.ISimpleService";

    private ISimpleService instance;

    @BeforeClass
    public static void beforeClass() throws NamingException
    {
        env = new Hashtable<>();
        env.put(Context.INITIAL_CONTEXT_FACTORY, WildFlyInitialContextFactory.class.getName() );
        env.put(Context.PROVIDER_URL, "http-remoting://" + host + ":" + port );
        env.put(Context.SECURITY_PRINCIPAL, "ck");
        env.put(Context.SECURITY_CREDENTIALS, "123");

        context = new InitialContext( env );
    }

    @Before
    public void setup() throws NamingException
    {

        Object r = context.lookup( JNDI_NAME );
        assertThat( r, is( not( nullValue() ) ) );
        instance = (ISimpleService) r;
    }

    @After
    public void tearDown()
    {
        instance = null;
    }

    @Test
    public void echo_test()
    {
        String message = "Hello simple services.";

        String response = instance.echo( message );

        assertThat( response, is( equalTo( message ) ) );
    }

    @Test
    public void lowercase_test()
    {
        String message = "HELLO random case SeRvIcE";
        String expected = "hello random case service";

        String response = instance.lowercase( message );

        assertThat( response, is( equalTo( expected ) ) );
    }


}
