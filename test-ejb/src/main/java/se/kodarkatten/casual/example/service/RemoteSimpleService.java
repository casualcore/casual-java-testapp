package se.kodarkatten.casual.example.service;

import se.kodarkatten.casual.example.service.remote.SomethingService;
import se.laz.casual.api.service.CasualService;
import se.laz.casual.jca.inbound.handler.InboundRequest;
import se.laz.casual.jca.inbound.handler.InboundResponse;

import javax.ejb.Remote;
import javax.ejb.Stateless;

@Stateless
@Remote(SomethingService.class)
public class RemoteSimpleService implements SomethingService
{

    @CasualService(name="RemoteTestEcho", category = "testing123" )
    @Override
    public InboundResponse casualEcho(InboundRequest buffer)
    {
        return InboundResponse.createBuilder()
                .buffer( buffer.getBuffer() )
                .build();
    }
}
