package se.kodarkatten.casual.example.service;

import se.laz.casual.api.buffer.CasualBuffer;
import se.laz.casual.api.service.CasualService;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

@Stateless(mappedName = "se.kodarkatten.casual.example.service.ISimpleService")
@Remote(ISimpleService.class)
public class SimpleService implements ISimpleService
{
    @Override
    public String echo(String message)
    {
        return message;
    }

    @Override
    public String lowercase(String message)
    {
        return message.toLowerCase();
    }

    @Override
    public SimpleObject echo(SimpleObject message)
    {
        return new SimpleObject( echo( message.getMessage() ) );
    }

    @Override
    public SimpleObject lowercase(SimpleObject message)
    {
        return new SimpleObject( lowercase( message.getMessage() ) );
    }

    @Override
    public CasualBuffer echo(CasualBuffer buffer)
    {
        return buffer;
    }

    @CasualService(name="TestCasualEcho", category = "testing123" )
    @TransactionAttribute(TransactionAttributeType.SUPPORTS)
    @Override
    public CasualBuffer casualEcho(CasualBuffer buffer)
    {
        return buffer;
    }
}
