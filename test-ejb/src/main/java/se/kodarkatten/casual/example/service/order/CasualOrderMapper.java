package se.kodarkatten.casual.example.service.order;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CasualOrderMapper
{
    public static Order toOrder(CasualOrder casualOrder )
    {
        Objects.requireNonNull( casualOrder, "CasualOrder is null." );
        Order order = new Order();
        order.setId( casualOrder.getId() );
        order.setVersion( casualOrder.getVersion() );
        order.setProduct( casualOrder.getProduct() );
        return order;
    }

    public static CasualOrder toCasualOrder(Order order)
    {
        Objects.requireNonNull( order, "Order is null." );
        CasualOrder casualOrder = new CasualOrder();
        casualOrder.setId( order.getId() );
        casualOrder.setVersion( order.getVersion() );
        casualOrder.setProduct( order.getProduct() );
        return casualOrder;
    }

    public static List<CasualOrder> toCasualOrder(List<Order> order )
    {
        return order.stream().map(CasualOrderMapper::toCasualOrder).collect(Collectors.toList());
    }
}
