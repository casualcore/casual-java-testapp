package se.kodarkatten.casual.example.service.order;

import se.laz.casual.api.service.CasualService;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless(mappedName = "se.kodarkatten.casual.example.service.order.ICasualOrderServiceRemote")
@Local(ICasualOrderService.class)
@Remote(ICasualOrderServiceRemote.class)
public class CasualOrderService implements ICasualOrderService
{
    @Inject
    private IOrderService orderService;

    @CasualService(name="TestCreateOrder")
    @Override
    public CasualOrder create(CasualOrder order)
    {
        Order o = CasualOrderMapper.toOrder( order );

        o = orderService.create( o );

        return CasualOrderMapper.toCasualOrder( o );
    }

    @Override
    public List<CasualOrder> get()
    {
        return CasualOrderMapper.toCasualOrder( orderService.get() );
    }

    @CasualService(name="TestGetOrderById" )
    @Override
    public CasualOrder get(CasualOrderId id)
    {
        return CasualOrderMapper.toCasualOrder( orderService.get( id.getId() ) );
    }

    @CasualService(name="TestUpdateOrder")
    @Override
    public CasualOrder update(CasualOrder order)
    {
        Order o = CasualOrderMapper.toOrder( order );
        o = orderService.update( o );
        return CasualOrderMapper.toCasualOrder( o );
    }

    @CasualService(name="TestDeleteOrderById")
    @Override
    public void delete(CasualOrderId id)
    {
        orderService.delete( id.getId() );
    }
}
