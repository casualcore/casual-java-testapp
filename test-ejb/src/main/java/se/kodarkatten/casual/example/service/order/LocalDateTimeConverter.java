package se.kodarkatten.casual.example.service.order;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;

@Converter
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Date>
{
    @Override
    public Date convertToDatabaseColumn(LocalDateTime attribute)
    {
        if( attribute == null )
        {
            return null;
        }
        return Date.from( attribute.atZone( ZoneId.systemDefault() ).toInstant() );
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Date dbData)
    {
        if( dbData == null )
        {
            return null;
        }
        return dbData.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
