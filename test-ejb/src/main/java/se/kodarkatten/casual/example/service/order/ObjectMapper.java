package se.kodarkatten.casual.example.service.order;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ObjectMapper
{
    public static Order toObject(OrderEntity entity )
    {
        Objects.requireNonNull( entity, "Entity is null." );
        Order order = new Order();
        order.setId( entity.getId() );
        order.setVersion( entity.getVersion() );
        order.setProduct( entity.getProduct() );
        order.setTstamp( entity.getTstamp() );
        return order;
    }

    public static List<Order> toObject( List<OrderEntity> entities )
    {
        return entities.stream().map(ObjectMapper::toObject).collect(Collectors.toList());
    }

    public static  OrderEntity toEntity(Order order)
    {
        Objects.requireNonNull( order, "Order is null." );
        OrderEntity entity = new OrderEntity();
        entity.setId( order.getId() );
        entity.setVersion( order.getVersion() );
        entity.setProduct( order.getProduct() );
        entity.setTstamp( order.getTstamp() );
        return entity;
    }
}
