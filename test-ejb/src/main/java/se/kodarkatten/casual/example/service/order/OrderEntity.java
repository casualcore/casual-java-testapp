package se.kodarkatten.casual.example.service.order;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Persistence;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table( name = "ORDERS" )
@SequenceGenerator( name = "ID_Generator", sequenceName = "ORDERIDSEQ", allocationSize = 1 )
@NamedQueries( { @NamedQuery( name = "getOrder", query = "SELECT o FROM OrderEntity o WHERE o.id = :id" ),
        @NamedQuery( name = "getAllOrders", query = "SELECT o FROM OrderEntity o" )} )
public class OrderEntity implements Serializable
{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ID_Generator")
    @Column( name = "ID", updatable = false )
    private Integer id;

    @Version
    @Column( name = "VERSION" )
    private Integer version;

    @Column( name = "PRODUCT" )
    private String product;

    @Column( name = "TSTAMP" )
    @Convert( converter = LocalDateTimeConverter.class )
    private LocalDateTime tstamp;

    @PrePersist
    @PreUpdate
    void onCreateOrPersist()
    {
        tstamp = LocalDateTime.now();
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getVersion()
    {
        return version;
    }

    public void setVersion(Integer version)
    {
        this.version = version;
    }

    public String getProduct()
    {
        return product;
    }

    public void setProduct(String product)
    {
        this.product = product;
    }

    public LocalDateTime getTstamp()
    {
        return tstamp;
    }

    public void setTstamp(LocalDateTime tstamp)
    {
        this.tstamp = tstamp;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        OrderEntity that = (OrderEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(version, that.version) &&
                Objects.equals(product, that.product) &&
                Objects.equals(tstamp, that.tstamp);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id, version, product, tstamp);
    }

    @Override
    public String toString()
    {
        return "OrderEntity{" +
                "id=" + id +
                ", version=" + version +
                ", product='" + product + '\'' +
                ", tstamp=" + tstamp +
                '}';
    }
}
