package se.kodarkatten.casual.example.service.order;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.logging.Logger;

@Stateless(mappedName = "se.kodarkatten.casual.example.service.order.IOrderServiceRemote" )
@Local(IOrderService.class)
@Remote(IOrderServiceRemote.class)
public class OrderService implements IOrderServiceRemote
{
    private static final Logger log = Logger.getLogger( OrderService.class.getName() );

    @PersistenceContext
    private EntityManager manager;

    @Override
    public Order create(Order order)
    {
        try
        {
            OrderEntity entity = ObjectMapper.toEntity( order );
            manager.persist(entity);
            manager.flush();
            return ObjectMapper.toObject( entity );
        }
        catch( RuntimeException e )
        {
            throw new OrderServiceException( "Error during creation.", e );
        }
    }

    @Override
    public List<Order> get()
    {
        try
        {
            TypedQuery<OrderEntity> query = manager.createNamedQuery("getAllOrders", OrderEntity.class);
            List<OrderEntity> entity = query.getResultList();
            return ObjectMapper.toObject( entity );
        }
        catch( RuntimeException e )
        {
            throw new OrderServiceException( "Error during get all orders.", e );
        }


    }

    @Override
    public Order get(Integer id)
    {
        try
        {
            TypedQuery<OrderEntity> query = manager.createNamedQuery("getOrder", OrderEntity.class);
            query.setParameter("id", id);

            OrderEntity entity = query.getSingleResult();

            return ObjectMapper.toObject(entity);
        }
        catch( NoResultException e )
        {
            throw new OrderNotFoundException( "Order not found with id: " + id, e );
        }
        catch( RuntimeException e )
        {
            throw new OrderServiceException( "Error during get with id: " + id, e );
        }
    }

    @Override
    public Order update(Order order)
    {
        try
        {
            OrderEntity entity = ObjectMapper.toEntity(order);

            entity = manager.merge(entity);
            manager.flush();

            return ObjectMapper.toObject(entity);
        }
        catch( NoResultException e )
        {
            throw new OrderNotFoundException( "Order not found with id: " + order.getId(), e );
        }
        catch( RuntimeException e )
        {
            throw new OrderServiceException( "Error during update with id: " + order.getId(), e );
        }

    }

    @Override
    public void delete(Integer id)
    {
        try
        {
            TypedQuery<OrderEntity> query = manager.createNamedQuery("getOrder", OrderEntity.class);
            query.setParameter("id", id );

            OrderEntity entity = query.getSingleResult();
            manager.remove( entity );
        }
        catch( NoResultException e )
        {
            throw new OrderNotFoundException( "Order not found with id: " + id, e );
        }
        catch( RuntimeException e )
        {
            throw new OrderServiceException( "Error during delete with id: " + id, e );
        }
    }
}
