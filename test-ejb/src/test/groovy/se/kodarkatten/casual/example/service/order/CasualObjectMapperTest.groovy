package se.kodarkatten.casual.example.service.order

import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalDateTime

class CasualObjectMapperTest extends Specification
{
    @Shared Order order
    @Shared CasualOrder casualOrder

    @Shared int id1 = 1
    @Shared int version1 = 2
    @Shared String product1 = "product name 1"
    @Shared LocalDateTime tstamp1 = LocalDateTime.of( 2017, 12, 11, 13, 14, 15 )

    def setup()
    {
        order = new Order()
        order.setId( id1 )
        order.setVersion( version1 )
        order.setProduct( product1 )

        casualOrder = new CasualOrder()
        casualOrder.setId( id1 )
        casualOrder.setVersion( version1 )
        casualOrder.setProduct( product1 )
    }

    def cleanup()
    {
        order = null
        casualOrder = null
    }

    def "casual order to order"()
    {
        when:
        Order actual = CasualOrderMapper.toOrder( casualOrder )

        then:
        actual == order
    }

    def "to order null, throws NullPointerException"()
    {
        when:
        CasualOrderMapper.toOrder( null )

        then:
        thrown NullPointerException.class
    }

    def "order to casual order"()
    {
        when:
        CasualOrder actual = CasualOrderMapper.toCasualOrder( order )

        then:
        actual == casualOrder
    }

    def "to entity null, throws NullPointerException"()
    {
        when:
        CasualOrderMapper.toCasualOrder( null )

        then:
        thrown NullPointerException.class
    }

    def "list order to list casual order"()
    {
        given:
        List<CasualOrder> expected = Arrays.asList( casualOrder, casualOrder )
        List<Order> input = Arrays.asList( order, order )

        when:
        List<CasualOrder> actual = CasualOrderMapper.toCasualOrder( input )

        then:
        actual == expected
    }
}

