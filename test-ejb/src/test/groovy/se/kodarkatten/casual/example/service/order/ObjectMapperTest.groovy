package se.kodarkatten.casual.example.service.order

import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalDateTime

class ObjectMapperTest extends Specification
{
    @Shared Order order
    @Shared OrderEntity entity

    @Shared int id1 = 1
    @Shared int version1 = 2
    @Shared String product1 = "product name 1"
    @Shared LocalDateTime tstamp1 = LocalDateTime.of( 2017, 12, 11, 13, 14, 15 )

    def setup()
    {
        order = new Order()
        order.setId( id1 )
        order.setVersion( version1 )
        order.setProduct( product1 )
        order.setTstamp( tstamp1 )

        entity = new OrderEntity()
        entity.setId( id1 )
        entity.setVersion( version1 )
        entity.setProduct( product1 )
        entity.setTstamp( tstamp1 )
    }

    def cleanup()
    {
        order = null
        entity = null
    }

    def "entity to object"()
    {
        when:
        Order actual = ObjectMapper.toObject( entity )

        then:
        actual == order
    }

    def "to object null, throws NullPointerException"()
    {
        when:
        ObjectMapper.toObject( null )

        then:
        thrown NullPointerException.class
    }

    def "pojo to entity"()
    {
        when:
        OrderEntity actual = ObjectMapper.toEntity( order )

        then:
        actual == entity
    }

    def "to entity null, throws NullPointerException"()
    {
        when:
        ObjectMapper.toEntity( null )

        then:
        thrown NullPointerException.class
    }

    def "list entity to list object"()
    {
        given:
        List<OrderEntity> input = Arrays.asList( entity, entity )
        List<Order> expected = Arrays.asList( order, order )

        when:
        List<Order> actual = ObjectMapper.toObject( input )

        then:
        actual == expected
    }
}

