package se.kodarkatten.casual.example.service.order

import spock.lang.Shared
import spock.lang.Specification

import javax.persistence.EntityManager
import javax.persistence.TypedQuery
import java.time.LocalDateTime

class OrderServiceTest extends Specification
{
    @Shared EntityManager persistence
    @Shared IOrderService instance

    @Shared Order order
    @Shared OrderEntity entity

    @Shared int id1 = 1
    @Shared int version1 = 2
    @Shared String product1 = "product name 1"
    @Shared LocalDateTime tstamp1 = LocalDateTime.of( 2017, 12, 11, 13, 14, 15 )

    def setup()
    {
        order = new Order()
        order.setId( id1 )
        order.setVersion( version1 )
        order.setProduct( product1 )
        order.setTstamp( tstamp1 )

        entity = new OrderEntity()
        entity.setId( id1 )
        entity.setVersion( version1 )
        entity.setProduct( product1 )
        entity.setTstamp( tstamp1 )

        persistence = Mock( EntityManager.class )
        instance = new OrderService()
        instance.manager = persistence
    }

    def cleanup()
    {
        instance = null
    }

    def "create forwards to manager."()
    {
        given:
        Order o = new Order()
        o.setProduct( product1 )
        OrderEntity actualEntity
        1 * persistence.persist( _ as OrderEntity ) >> { OrderEntity e ->
            actualEntity = new OrderEntity()
            actualEntity.setId( e.getId() )
            actualEntity.setVersion( e.getVersion() )
            actualEntity.setProduct( e.getProduct() )
            actualEntity.setTstamp( e.getTstamp() )

            e.setId( entity.getId() )
            e.setVersion( entity.getVersion() )
            e.setTstamp( entity.getTstamp() )
        }

        when:
        Order actual = instance.create( o )

        then:
        1 * persistence.flush()
        actual == order
    }

    def "get forwards to manager"()
    {
        given:
        TypedQuery<OrderEntity> query = Mock( TypedQuery.class )
        1 * query.getSingleResult() >> {
            return entity
        }

        String actualQuery
        Class actualClass
        1 * persistence.createNamedQuery( _ as String, _ as Class<OrderEntity> ) >> { String q, Class<OrderEntity> c ->
            actualQuery = q
            actualClass = c
            return query
        }

        when:
        Order actual = instance.get( order.getId() )

        then:
        actual == order

        1 * query.setParameter( "id", order.getId() )

        actualQuery == "getOrder"
        actualClass == OrderEntity.class
    }

    def "get all forwards to manager"()
    {
        given:
        TypedQuery<OrderEntity> query = Mock( TypedQuery.class )
        1 * query.getResultList() >> {
            return Arrays.asList( entity, entity )
        }

        String actualQuery
        Class actualClass
        1 * persistence.createNamedQuery( _ as String, _ as Class<OrderEntity> ) >> { String q, Class<OrderEntity> c ->
            actualQuery = q
            actualClass = c
            return query
        }

        when:
        List<Order> actual = instance.get( )

        then:
        actual == Arrays.asList( order, order )

        actualQuery == "getAllOrders"
        actualClass == OrderEntity.class
    }

    def "update forwards to manager."()
    {
        given:
        Order o = order
        o.setProduct( product1 )
        OrderEntity actualEntity
        1 * persistence.merge( _ as OrderEntity ) >> { OrderEntity e ->
            actualEntity = new OrderEntity()
            actualEntity.setId( e.getId() )
            actualEntity.setVersion( e.getVersion() )
            actualEntity.setProduct( e.getProduct() )
            actualEntity.setTstamp( e.getTstamp() )

            OrderEntity r = new OrderEntity()
            r.setId( e.getId() )
            r.setVersion( e.getVersion() )
            r.setProduct( e.getProduct() )
            r.setTstamp( entity.getTstamp() )
            return r
        }

        when:
        Order actual = instance.update( o )

        then:
        1 * persistence.flush()
        actual == order
    }

    def "delete forwards to manager"()
    {
        given:
        TypedQuery<OrderEntity> query = Mock( TypedQuery.class )
        1 * query.getSingleResult() >> {
            return entity
        }

        String actualQuery
        Class actualClass
        1 * persistence.createNamedQuery( _ as String, _ as Class<OrderEntity> ) >> { String q, Class<OrderEntity> c ->
            actualQuery = q
            actualClass = c
            return query
        }

        when:
        instance.delete( order.getId() )

        then:
        1 * query.setParameter( "id", order.getId() )
        1 * persistence.remove( entity )

        actualQuery == "getOrder"
        actualClass == OrderEntity.class
    }

}
