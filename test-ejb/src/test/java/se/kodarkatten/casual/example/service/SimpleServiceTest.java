package se.kodarkatten.casual.example.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class SimpleServiceTest
{
    ISimpleService instance = null;
    String message = "This is the message.";
    String lowerCaseMessage = "this is the message.";
    SimpleObject simpleObject;

    @Before
    public void setUp() throws Exception
    {
        instance = new SimpleService();
        simpleObject = new SimpleObject( message );
    }

    @After
    public void tearDown() throws Exception
    {
        instance = null;
    }

    @Test
    public void echo() throws Exception
    {
        String actual = instance.echo( message );

        assertThat( actual, is( equalTo( message ) ) );
    }

    @Test
    public void lowercase() throws Exception
    {
        String actual = instance.lowercase( message );
        assertThat( actual, is( equalTo( lowerCaseMessage ) ) );
    }

    @Test
    public void echoObject() throws Exception
    {
        SimpleObject actual = instance.echo( simpleObject );

        assertThat( actual, is( equalTo( simpleObject ) ) );
    }

    @Test
    public void lowercaseObject() throws Exception
    {
        SimpleObject expected = new SimpleObject( lowerCaseMessage );

        SimpleObject actual = instance.lowercase( simpleObject );

        assertThat( actual, is( equalTo( expected ) ) );
    }

}