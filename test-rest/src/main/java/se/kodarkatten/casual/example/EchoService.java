package se.kodarkatten.casual.example;

/**
 * @author jone
 */

import se.laz.casual.api.buffer.CasualBuffer;
import se.laz.casual.api.buffer.ServiceReturn;
import se.laz.casual.api.buffer.type.JsonBuffer;
import se.laz.casual.api.buffer.type.fielded.FieldedTypeBuffer;
import se.laz.casual.api.flags.AtmiFlags;
import se.laz.casual.api.flags.Flag;
import se.laz.casual.api.flags.ServiceReturnState;
import se.laz.casual.jca.CasualConnection;
import se.laz.casual.jca.CasualConnectionFactory;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Call such as
 * http://morpheus.neverwhere:7001/casual-java-testapp/example/echo/bazinga?bufferType=fielded
 */
@Stateless
@Path("/echo")
public class EchoService
{
    // for jboss
    @Resource(lookup = "java:/eis/casualConnectionFactory")
    // for WLS
    //@Resource(lookup = "eis/casualConnectionFactory")
    private CasualConnectionFactory conFac;

    @Resource
    private EJBContext ctx;

    private static final String JSON_BUFFER_TYPE = "json";
    private static final String FIELDED_BUFFER_TYPE = "fielded";

    @GET
    @Path("{data}")
    public String echoRequest(@PathParam("data") String data, @QueryParam("bufferType") @DefaultValue(JSON_BUFFER_TYPE) String bufferType)
    {
        try(CasualConnection connection = conFac.getConnection())
        {
            if(JSON_BUFFER_TYPE.equals(bufferType))
            {
                return makeJSONCall(connection, data);
            }
            if(FIELDED_BUFFER_TYPE.equals(bufferType))
            {
                return makeFieldedCall(connection, data);
            }
            throw new RuntimeException("unknown buffertype: " + bufferType);
        }
        catch (Exception e)
        {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            if(!ctx.getRollbackOnly())
            {
                ctx.setRollbackOnly();
            }
            return sw.toString(); // stack trace as a string
        }
    }

    private static String makeJSONCall(final CasualConnection connection , final String data)
    {
        CasualBuffer msg = JsonBuffer.of("{\"msg\": \"" + data + "\"}");

        ServiceReturn<CasualBuffer> reply = connection.tpcall("casual/example/echo", msg, Flag.of(AtmiFlags.NOFLAG));

        if(reply.getServiceReturnState() == ServiceReturnState.TPSUCCESS)
        {
            return JsonBuffer.of(reply.getReplyBuffer().getBytes()).toString();
        }
        throw new RuntimeException("tpcall failed: " + reply.getErrorState());
    }

    private static String makeFieldedCall(final CasualConnection connection , final String data)
    {
        StringBuilder b = new StringBuilder(data);
        CasualBuffer msg = FieldedTypeBuffer.create()
                                            .write("FML_STRING1", data)
                                            .write("FML_STRING1", b.reverse().toString());

        ServiceReturn<CasualBuffer> reply = connection.tpcall("casual/example/echo", msg, Flag.of(AtmiFlags.NOFLAG));

        if(reply.getServiceReturnState() == ServiceReturnState.TPSUCCESS)
        {
            FieldedTypeBuffer replyFielded = FieldedTypeBuffer.create(reply.getReplyBuffer().getBytes());
            return replyFielded.toString();
        }
        throw new RuntimeException("tpcall failed: " + reply.getErrorState());
    }
}
