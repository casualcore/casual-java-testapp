package se.kodarkatten.casual.example;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author jone
 */
@ApplicationPath("example")
public class JAXRSConfiguration extends Application
{
}
