package se.kodarkatten.casual.example;

import se.laz.casual.api.buffer.CasualBuffer;
import se.laz.casual.api.buffer.type.JsonBuffer;
import se.laz.casual.api.queue.MessageSelector;
import se.laz.casual.api.queue.QueueInfo;
import se.laz.casual.api.queue.QueueMessage;
import se.laz.casual.jca.CasualConnection;
import se.laz.casual.jca.CasualConnectionFactory;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * With qspace casual and queuename testQueue
 * call such as:
 *
 * enqueue
 * http://127.0.0.1:7003/casual-java-testapp/example/queue/enqueue/bazinga?queuespace=casual&queuename=testQueue
 *
 * dequeue
 * http://127.0.0.1:7003/casual-java-testapp/example/queue/dequeue?queuespace=casual&queuename=testQueue
 * or with a specific uuid
 * http://127.0.0.1:7003/casual-java-testapp/example/queue/dequeue?queuespace=casual&queuename=testQueue&uid=7a92187f-e62c-464a-ba31-64f1119b1ac1
 */
@Stateless
@Path("/queue")
public class QueueService
{
    // for jboss
    @Resource(lookup = "java:/eis/casualConnectionFactory")
    // wls
    //@Resource(lookup = "eis/casualConnectionFactory")
    private CasualConnectionFactory conFac;

    @Resource
    private EJBContext ctx;

    private static final AtomicInteger nrFails = new AtomicInteger();
    private static final Logger log = Logger.getLogger(QueueService.class.getName());

    @GET
    @Path("/enqueue/{data}")
    public String enqueueRequest(@PathParam("data") String data, @QueryParam("queuename") String qname, @QueryParam("queuespace") String qspace)
    {
        try(CasualConnection connection = conFac.getConnection())
        {
            if(null == qspace || qspace.isEmpty())
            {
                throw new IllegalArgumentException("qspace must be specified");
            }
            if(null == qname || qname.isEmpty())
            {
                throw new IllegalArgumentException("qname must be specified");
            }
            CasualBuffer msg = JsonBuffer.of("{\"msg\": \"" + data + "\"}");
            UUID msgUIID = connection.enqueue(QueueInfo.createBuilder()
                                                       .withQspace(qspace)
                                                       .withQname(qname)
                                                       .build(), QueueMessage.of(msg));
            return "msg enqueued with id: " + msgUIID.toString();
        }
        catch (Exception e)
        {
            log.warning(() -> "nrFails: " + nrFails.incrementAndGet() + " exception: " + e);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            if(!ctx.getRollbackOnly())
            {
                ctx.setRollbackOnly();
            }
            return sw.toString(); // stack trace as a string
        }
    }

    @GET
    @Path("/dequeue")
    public String dequeueRequest(@QueryParam("queuename") String qname, @QueryParam("queuespace") String qspace, @QueryParam("uid") String uid)
    {
        if (null == qspace || qspace.isEmpty())
        {
            throw new IllegalArgumentException("qspace must be specified");
        }
        if (null == qname || qname.isEmpty())
        {
            throw new IllegalArgumentException("qname must be specified");
        }
        try (CasualConnection connection = conFac.getConnection())
        {
            UUID uuid = (null != uid && !uid.isEmpty()) ? UUID.fromString(uid) : new UUID(0,0);
            List<QueueMessage> l = makeDequeueRequestWithQSpaceAndQName(connection, qspace, qname, uuid);
            return "dequeued messages: " + l.stream()
                                            .map(s -> JsonBuffer.of(s.getPayload().getBytes()).toString())
                                            .collect(Collectors.joining("\n\n"));
        }
        catch (Exception e)
        {
            log.warning(() -> "nrFails: " + nrFails.incrementAndGet() + " exception: " + e);
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            if (!ctx.getRollbackOnly())
            {
                ctx.setRollbackOnly();
            }
            return sw.toString(); // stack trace as a string
        }
    }

    private List<QueueMessage> makeDequeueRequestWithQSpaceAndQName(CasualConnection connection, String qspace, String qname, UUID uuid)
    {
        return connection.dequeue(QueueInfo.createBuilder()
                                           .withQspace(qspace)
                                           .withQname(qname)
                                           .build(), MessageSelector.of(uuid));
    }

}
