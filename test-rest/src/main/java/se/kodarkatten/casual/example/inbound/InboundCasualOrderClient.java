package se.kodarkatten.casual.example.inbound;

import se.laz.casual.api.buffer.CasualBuffer;
import se.laz.casual.api.buffer.ServiceReturn;
import se.laz.casual.api.buffer.type.fielded.FieldedTypeBuffer;
import se.laz.casual.api.buffer.type.fielded.marshalling.FieldedTypeBufferProcessor;
import se.laz.casual.api.flags.AtmiFlags;
import se.laz.casual.api.flags.Flag;
import se.laz.casual.api.flags.ServiceReturnState;
import se.kodarkatten.casual.example.service.order.CasualOrder;
import se.kodarkatten.casual.example.service.order.CasualOrderId;
import se.laz.casual.jca.CasualConnection;
import se.laz.casual.jca.CasualConnectionFactory;

import javax.annotation.Resource;
import javax.resource.ResourceException;
import javax.transaction.xa.XAException;
import java.util.ArrayList;
import java.util.List;

public class InboundCasualOrderClient
{
    private static final String CREATE_ORDER_SERVICE_NAME = "TestCreateOrder";
    private static final String GET_ORDER_BY_ID_SERVICE_NAME = "TestGetOrderById";
    private static final String GET_ALL_ORDERS_SERVICE_NAME = "TestGetAllOrders";
    private static final String UPDATE_ORDER_SERVICE_NAME = "TestUpdateOrder";
    private static final String DELETE_ORDER_SERVICE_NAME = "TestDeleteOrderById";

    //WILDFLY
    @Resource( name ="java:/eis/inboundConnectionFactory" )
    //WEBLOGIC
    //@Resource( name ="eis/inboundConnectionFactory" )
    private CasualConnectionFactory connectionFactory;

    public CasualOrder create(CasualOrder order ) throws ResourceException, XAException
    {
        FieldedTypeBuffer buffer = FieldedTypeBufferProcessor.marshall( order );

        try( CasualConnection connection = connectionFactory.getConnection())
        {
            CasualBuffer msg = buffer;

            ServiceReturn<CasualBuffer> reply = connection.tpcall(CREATE_ORDER_SERVICE_NAME, msg, Flag.of(AtmiFlags.NOFLAG));


            if (reply.getServiceReturnState() != ServiceReturnState.TPSUCCESS)
            {
                throw new RuntimeException("Reply status: " + reply.getServiceReturnState());
            }

            CasualOrder actual = FieldedTypeBufferProcessor.unmarshall(FieldedTypeBuffer.create(reply.getReplyBuffer().getBytes()), CasualOrder.class);

            return actual;
        }
    }

    public List<CasualOrder> get() throws ResourceException, XAException
    {
        //Not sure how to unmarshall a list from a response service buffer.
        return new ArrayList<>();
    }

    public CasualOrder get( CasualOrderId orderId ) throws ResourceException, XAException
    {
        FieldedTypeBuffer buffer = FieldedTypeBufferProcessor.marshall( orderId );

        try( CasualConnection connection = connectionFactory.getConnection())
        {
            CasualBuffer msg = buffer;

            ServiceReturn<CasualBuffer> reply = connection.tpcall(GET_ORDER_BY_ID_SERVICE_NAME, msg, Flag.of(AtmiFlags.NOFLAG));

            if (reply.getServiceReturnState() != ServiceReturnState.TPSUCCESS)
            {
                throw new RuntimeException("Reply status: " + reply.getServiceReturnState());
            }

            CasualOrder actual = FieldedTypeBufferProcessor.unmarshall(FieldedTypeBuffer.create(reply.getReplyBuffer().getBytes()), CasualOrder.class);

            return actual;
        }
    }

    public CasualOrder update( CasualOrder order ) throws ResourceException, XAException
    {
        FieldedTypeBuffer buffer = FieldedTypeBufferProcessor.marshall( order );

        try( CasualConnection connection = connectionFactory.getConnection())
        {
            CasualBuffer msg = buffer;

            ServiceReturn<CasualBuffer> reply = connection.tpcall(UPDATE_ORDER_SERVICE_NAME, msg, Flag.of(AtmiFlags.NOFLAG));

            if (reply.getServiceReturnState() != ServiceReturnState.TPSUCCESS)
            {
                throw new RuntimeException("Reply status: " + reply.getServiceReturnState());
            }

            CasualOrder actual = FieldedTypeBufferProcessor.unmarshall(FieldedTypeBuffer.create(reply.getReplyBuffer().getBytes()), CasualOrder.class);

            return actual;
        }
    }

    public void delete( CasualOrderId orderId ) throws ResourceException, XAException
    {
        FieldedTypeBuffer buffer = FieldedTypeBufferProcessor.marshall( orderId );

        try( CasualConnection connection = connectionFactory.getConnection())
        {
            CasualBuffer msg = buffer;

            ServiceReturn<CasualBuffer> reply = connection.tpcall(DELETE_ORDER_SERVICE_NAME, msg, Flag.of(AtmiFlags.NOFLAG));

            if (reply.getServiceReturnState() != ServiceReturnState.TPSUCCESS)
            {
                throw new RuntimeException("Reply status: " + reply.getServiceReturnState());
            }
        }
    }
}
