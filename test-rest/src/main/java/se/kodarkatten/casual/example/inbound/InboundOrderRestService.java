package se.kodarkatten.casual.example.inbound;

import se.kodarkatten.casual.example.service.order.CasualOrder;
import se.kodarkatten.casual.example.service.order.CasualOrderId;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.util.List;

@Stateless
@Path("/inbound/orders")
public class InboundOrderRestService
{
    @Inject
    private InboundCasualOrderClient orderClient;

    @Resource
    private EJBContext ctx;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("order/{id}")
    public Response get(@PathParam("id") Integer id )
    {
        try
        {
            CasualOrderId orderId = new CasualOrderId();
            orderId.setId( id );
            CasualOrder o = orderClient.get(orderId);
            return Response.ok().entity( o ).build();
        }
        catch( Exception e )
        {
            markForRollback();
            return Response.serverError().entity( exeptionToString(e) ).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get()
    {
        try
        {
            List<CasualOrder> orders = orderClient.get();
            return Response.ok().entity( orders ).build();
        }
        catch( Exception e )
        {
            markForRollback();
            return Response.serverError().entity( exeptionToString(e) ).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create( CasualOrder order )
    {
        try
        {
            CasualOrder o = orderClient.create(order);
            return Response.created( URI.create( "/inbound/orders/order/" + o.getId() ) ).entity( o ).build();
        }
        catch( Exception e )
        {
            markForRollback();
            return Response.serverError().entity( exeptionToString(e) ).build();
        }

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("order/{id}")
    public Response update( CasualOrder order )
    {
        try
        {
            CasualOrder o = orderClient.update(order);
            return Response.ok().entity( o ).build();
        }
        catch( Exception e )
        {
            markForRollback();
            return Response.serverError().entity( exeptionToString(e) ).build();
        }
    }

    @DELETE
    @Path("order/{id}")
    public Response delete( @PathParam("id")Integer id )
    {
        try
        {
            CasualOrderId orderId = new CasualOrderId();
            orderId.setId( id );
            orderClient.delete(orderId);
            return Response.ok().build();
        }
        catch( Exception e )
        {
            markForRollback();
            return Response.serverError().entity( exeptionToString(e) ).build();
        }
    }

    private String exeptionToString( Throwable e )
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace( pw );
        return sw.toString();

    }

    private void markForRollback()
    {
        if(!ctx.getRollbackOnly())
        {
            ctx.setRollbackOnly();
        }
    }

}
