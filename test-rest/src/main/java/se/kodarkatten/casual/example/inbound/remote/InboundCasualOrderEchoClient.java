package se.kodarkatten.casual.example.inbound.remote;

import se.kodarkatten.casual.example.service.order.CasualOrderId;
import se.laz.casual.api.buffer.CasualBuffer;
import se.laz.casual.api.buffer.ServiceReturn;
import se.laz.casual.api.buffer.type.fielded.FieldedTypeBuffer;
import se.laz.casual.api.buffer.type.fielded.marshalling.FieldedTypeBufferProcessor;
import se.laz.casual.api.flags.AtmiFlags;
import se.laz.casual.api.flags.Flag;
import se.laz.casual.api.flags.ServiceReturnState;
import se.laz.casual.jca.CasualConnection;
import se.laz.casual.jca.CasualConnectionFactory;

import javax.annotation.Resource;
import javax.resource.ResourceException;

public class InboundCasualOrderEchoClient
{
    private static final String ECHO_SERVICE_NAME = "RemoteTestEcho";

    @Resource( name ="java:/eis/inboundConnectionFactory" )
    private CasualConnectionFactory connectionFactory;

    public CasualOrderId echo(CasualOrderId order ) throws ResourceException
    {
        FieldedTypeBuffer buffer = FieldedTypeBufferProcessor.marshall( order );

        try( CasualConnection connection = connectionFactory.getConnection())
        {
            CasualBuffer msg = buffer;

            ServiceReturn<CasualBuffer> reply = connection.tpcall(ECHO_SERVICE_NAME, msg, Flag.of(AtmiFlags.NOFLAG));


            if (reply.getServiceReturnState() != ServiceReturnState.TPSUCCESS)
            {
                throw new RuntimeException("Reply status: " + reply.getServiceReturnState());
            }

            CasualOrderId actual = FieldedTypeBufferProcessor.unmarshall(FieldedTypeBuffer.create(reply.getReplyBuffer().getBytes()), CasualOrderId.class);

            return actual;
        }
    }
}
