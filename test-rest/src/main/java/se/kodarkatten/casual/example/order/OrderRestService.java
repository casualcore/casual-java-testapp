package se.kodarkatten.casual.example.order;

import se.kodarkatten.casual.example.service.order.IOrderService;
import se.kodarkatten.casual.example.service.order.Order;
import se.kodarkatten.casual.example.service.order.OrderNotFoundException;
import se.kodarkatten.casual.example.service.order.OrderServiceException;

import javax.ejb.ObjectNotFoundException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.util.List;

@Stateless
@Path("/orders")
public class OrderRestService
{
    @Inject
    private IOrderService orderService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("order/{id}")
    public Response get(@PathParam("id") Integer id )
    {
        try
        {
            Order o = orderService.get(id);
            return Response.ok().entity( o ).build();
        }
        catch( OrderNotFoundException e )
        {
            return Response.status(Response.Status.NOT_FOUND ).build();
        }
        catch( OrderServiceException e )
        {
            return Response.serverError().entity( exeptionToString(e) ).build();
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get()
    {
        try
        {
            List<Order> orders = orderService.get();
            return Response.ok().entity( orders ).build();
        }
        catch( OrderServiceException e )
        {
            return Response.serverError().entity( exeptionToString(e) ).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create( Order order )
    {
        try
        {
            Order o = orderService.create(order);
            return Response.created( URI.create( "/orders/order/" + o.getId() ) ).entity( o ).build();
        }
        catch( OrderServiceException e )
        {
            return Response.serverError().entity( exeptionToString(e) ).build();
        }

    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("order/{id}")
    public Response update( Order order )
    {
        try
        {
            Order o = orderService.update(order);
            return Response.ok().entity( o ).build();
        }
        catch( OrderNotFoundException e )
        {
            return Response.status(Response.Status.NOT_FOUND ).build();
        }
        catch( OrderServiceException e )
        {
            return Response.serverError().entity( exeptionToString(e) ).build();
        }
    }

    @DELETE
    @Path("order/{id}")
    public Response delete( @PathParam("id")Integer id )
    {
        try
        {
            orderService.delete(id);
            return Response.ok().build();
        }
        catch( OrderNotFoundException e )
        {
            return Response.status(Response.Status.NO_CONTENT ).build();
        }
        catch( OrderServiceException e )
        {
            return Response.serverError().entity( exeptionToString(e) ).build();
        }
    }

    private String exeptionToString( Throwable e )
    {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace( pw );
        return sw.toString();

    }
}
